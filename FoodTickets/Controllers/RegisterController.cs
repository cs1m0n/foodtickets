﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FoodTickets.Models;

namespace FoodTickets.Controllers
{
    public class RegisterController : Controller
    {
        FoodTicketsEntities db = new FoodTicketsEntities();
        // GET: Register
        public ActionResult SetDataInDataBase()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SetDataInDataBase(Meal model)
        {
            Meal tbl = new Meal();
            tbl.PersonID = model.PersonID;
            tbl.MealType = model.MealType;
            tbl.Date = model.Date;
            db.Meals.Add(tbl);
            db.SaveChanges();
            return View();
        }

        public ActionResult ShowDataBaseForUser()
        {
            var item = db.Meals.ToList();
            return View(item);
        }
        public ActionResult Delete(int id)
        {
            var item = db.Meals.Where(x => x.MealID == id).First();
            db.Meals.Remove(item);
            db.SaveChanges();
            var item2 = db.Meals.ToList();
            return View("ShowDataBaseForUser", item2);
        }
        public ActionResult Edit(int id)
        {
            var item = db.Meals.Where(x => x.MealID == id).First();
            if(item.Taken == 1)
            {
                return View("AlreadyTaken");
            } else
            {
                item.Taken = 1;
                db.SaveChanges();
                return View("Succes");            
                //return View(item);
            }
        }
        [HttpPost]
        public ActionResult Edit(Meal model)
        {
            var item = db.Meals.Where(x => x.MealID == model.MealID).First();
            item.PersonID = model.PersonID;
            item.MealType = model.MealType;
            item.Date = model.Date;
            item.Taken = model.Taken;          
            db.SaveChanges();
            return View();
        }
    }
}